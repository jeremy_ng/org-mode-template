# README #

### What is this repository for? ###

This is the Org-mode format that was used for my HT submission to NUS department of biological science, with a custom cover page and miscs. 

### How do I get set up? ###
The main body of the thesis is formatted in the org mode file. All other components goes into their respective .tex files.
Also, you would need to change the title, and provide details such as your matriculation number, semester, and degrees in the file title.tex, which then makes the custom title page. I'm trying to find a way to have the title and the author passed directly from the org-mode exporter, but to no success yet ...

Feel free to contact me at jeremy.ng.wk1990@gmail.com if anything~ 